import nodeApi from './node.api';

import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';

const mockReactStaticArgs = {
  stage: 'node' as 'node',
  defaultLoaders: {
    jsLoader: { loader: 'js', use: [{ options: {} }] },
    jsLoaderExt: { loader: 'jsExt' },
    cssLoader: { loader: 'css' },
    fileLoader: { loader: 'file' },
  },
};

it('should add a loader for .tsx and .ts files', () => {
  const webpackConfig = {};

  const updatedWebpackConfig = nodeApi().webpack(webpackConfig, mockReactStaticArgs);

  const fileMatchRegEx = updatedWebpackConfig.module.rules[0].oneOf[0].test as RegExp;
  expect(fileMatchRegEx.test('typeScriptFile.tsx')).toBe(true);
  expect(fileMatchRegEx.test('typeScriptFile.ts')).toBe(true);
});

it('should preserve existing Babel presets', () => {
  const webpackConfig = {};

  const mockReactStaticArgsWithPresets = {
    ...mockReactStaticArgs,
    defaultLoaders: {
      ...mockReactStaticArgs.defaultLoaders,
      jsLoader: { use: [ { options: { presets: ['mock-preset'] } } ] },
    },
  };

  const updatedWebpackConfig: any = nodeApi().webpack(webpackConfig, mockReactStaticArgsWithPresets);

  const presets = updatedWebpackConfig.module.rules[0].oneOf[0].use[0].options.presets as string[];
  expect(presets).toEqual(['mock-preset', '@babel/preset-typescript']);
});

it('should preserve the default loaders', () => {
  const webpackConfig = {};

  const updatedWebpackConfig = nodeApi().webpack(webpackConfig, mockReactStaticArgs);

  expect(updatedWebpackConfig.module.rules[0].oneOf[0])
    .not.toEqual(mockReactStaticArgs.defaultLoaders.jsLoader);
  expect(updatedWebpackConfig.module.rules[0].oneOf[1])
    .toEqual(mockReactStaticArgs.defaultLoaders.jsLoaderExt);
  expect(updatedWebpackConfig.module.rules[0].oneOf[2])
    .toEqual(mockReactStaticArgs.defaultLoaders.cssLoader);
  expect(updatedWebpackConfig.module.rules[0].oneOf[3])
    .toEqual(mockReactStaticArgs.defaultLoaders.fileLoader);
});

it('should add the TypeScript loader even if the default Javascript loader is not present', () => {
  const webpackConfig = {
    module: {
      rules: [{
        oneOf: [ { loader: 'arbitrary non-default loader' } ],
      }],
    },
  };

  const updatedWebpackConfig = nodeApi().webpack(webpackConfig, mockReactStaticArgs);

  expect(updatedWebpackConfig.module.rules[0].oneOf.length).toBe(2);
  const fileMatchRegEx = updatedWebpackConfig.module.rules[0].oneOf[0].test as RegExp;
  expect(fileMatchRegEx.test('typeScriptFile.tsx')).toBe(true);
  expect(fileMatchRegEx.test('typeScriptFile.ts')).toBe(true);
});

it('should preserve other custom loaders', () => {
  const modifiedLoaders = [
    { loader: 'modified-css-loader' },
    mockReactStaticArgs.defaultLoaders.jsLoader,
    mockReactStaticArgs.defaultLoaders.jsLoaderExt,
    mockReactStaticArgs.defaultLoaders.cssLoader,
    mockReactStaticArgs.defaultLoaders.fileLoader,
  ];
  const webpackConfig = {
    module: {
      rules: [{
        oneOf: modifiedLoaders,
      }],
    },
  };

  const updatedWebpackConfig = nodeApi().webpack(webpackConfig, mockReactStaticArgs);

  expect(updatedWebpackConfig.module.rules[0].oneOf[0])
    .toEqual(modifiedLoaders[0]);
});

it('should tell React Static that pages can have .ts or .tsx extensions', () => {
  const reactStaticConfig = { extensions: [] };

  const updatedReactStaticConfig = nodeApi().config(reactStaticConfig);

  expect(updatedReactStaticConfig.extensions).toEqual(['.ts', '.tsx']);
});

it('should preserve existing extensions for pages', () => {
  const reactStaticConfig = { extensions: [ '.1337' ] };

  const updatedReactStaticConfig = nodeApi().config(reactStaticConfig);

  expect(updatedReactStaticConfig.extensions).toEqual(['.1337', '.ts', '.tsx']);
});

// Type checking will be enabled by default later, but since it's a breaking change, not now:
it('should add type checking by default', () => {
  const webpackConfig = {};

  const updatedWebpackConfig = nodeApi().webpack(webpackConfig, mockReactStaticArgs);

  expect(updatedWebpackConfig.plugins.length).toBe(1);
  expect(updatedWebpackConfig.plugins[0]).toBeInstanceOf(ForkTsCheckerWebpackPlugin);
});

it('should not add type checking when it was explicitly disabled', () => {
  const webpackConfig = {};

  const updatedWebpackConfig = nodeApi({ typeCheck: false })
    .webpack(webpackConfig, mockReactStaticArgs);

  expect(updatedWebpackConfig.plugins).toEqual([]);
});

it('should add type checking when it is explicitly enabled', () => {
  const webpackConfig = {};

  const updatedWebpackConfig = nodeApi({ typeCheck: true })
    .webpack(webpackConfig, mockReactStaticArgs);

  expect(updatedWebpackConfig.plugins.length).toBe(1);
  expect(updatedWebpackConfig.plugins[0]).toBeInstanceOf(ForkTsCheckerWebpackPlugin);
});
